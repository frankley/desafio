@extends('layouts.app')
@section('content')
<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-md">
                <h4 class="font-weight-bold py-3 mb-0">
                    <span class="text-muted font-weight-light">Home /</span> Dicas
                </h4>
            </div>
            <div class="col-md">
                <form id="form-home-filtro">
                    <div class="form-group">
                        <div class="input-group">
                        <select class="form-control" name="marca_id" id="filtro-home-marca_id">
                            <option value="" selected disabled>Selecione a Marca</option>
                            @foreach ($marcas as $mar)
                            <option value="{{$mar->id}}">{{$mar->nome}}</option>
                            @endforeach
                        </select>
                        <select class="form-control d-none" name="modelo_id" id="filtro-home-modelo_id">
                            <option value="" selected disabled>Selecione o Modelo</option>
                        </select>
                        <span class="input-group-append">
                            <button class="btn btn-secondary" type="submit">Buscar</button>
                        </span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="row box-dicas-home"></div>
        <div class="paginacao">

        </div>
    </div>
    
    <div id="data-container"></div>
    <div id="pagination-container"></div>

</div>   

<script>
$(document).ready(function(){
    minhasDicasHome();
})

function minhasDicasHome(form){
    $(".box-dicas-home").html("");
    $.ajax({
        type: "GET",
        url: "{{url('lista')}}",
        data: form,
        success: function(data){
            // console.log(data.data);
            if(data != ''){
                $(".box-dicas-home").html(data)
            }else{
                $(".box-dicas-home").html("Nenhum veiculo dessa Marca Cadastrada.");
            }
        }
    })
}


$('#filtro-home-marca_id').change(function(){
    dicaModelos($(this).val());
})

function dicaModelos(marca_id){
    $("#filtro-home-modelo_id").html('<option disabled selected>Selecione a marca...</option>')
    $('#filtro-home-modelo_id').addClass('d-none');
    $.ajax({
        type: "GET",
        url: "{{url('modelos/lista')}}",
        data: {
            marca_id: marca_id
        },
        success: function(data){
            if(data != ''){
                $('#filtro-home-modelo_id').removeClass('d-none');
                $(data).each(function(key, dados){
                    $("#filtro-home-modelo_id").append("<option value='"+dados.modelo_id+"'>"+dados.nome+' - '+dados.ano+' - '+dados.combustivel+"</option>");
                })
            }else{
                $('#filtro-home-modelo_id').addClass('d-none');
            }
        }
    })
}

$("#form-home-filtro").on("submit", function(e){
    e.preventDefault();
    let form = $(this).serialize();
    minhasDicasHome(form)
})
</script>
@endsection