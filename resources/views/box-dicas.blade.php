<table class="table table-striped table-responsive" id="datatables-box-dicas">
        <thead>
            <th></th>
        </thead>
        <tbody>
            @foreach ($dicas as $dica)
            <tr>
                <td>
                    <div class="col-md card">
                        <div class="card-header"><a href="javascript:void(0)" class="text-body text-large font-weight-semibold">{{$dica->modelo}} - {{$dica->ano}} - {{$dica->combustivel}}</a></div>
                        <div class="d-flex flex-wrap mt-0 card-body" style="flex:0">
                            <div class="mr-3"><i class="vacancy-tooltip ion ion-md-car text-light"></i>&nbsp;{{$dica->marca}}</div>
                            <div class="mr-3"><i class="vacancy-tooltip ion ion-md-time text-primary" title="Employment"></i>&nbsp;{{$dica->created}}</div>
                        </div>
                        <div class="mr-3 mt-0 mb-1">
                            {{$dica->descricao}}
                        </div>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
</table>

<script>
$(document).ready(function(){
    boxDicas();
});
function boxDicas(){
    $.fn.dataTable.ext.errMode = 'throw';
    if ( $.fn.dataTable.isDataTable( '#datatables-box-dicas' ) ) {
            mainTable.destroy();
    }

    mainTable = $('#datatables-box-dicas').DataTable( {
            language: {
                url: "{{asset('assets/localisation/Portuguese-Brasil.json')}}"    
            },
            pageLength: 3,
            dom: 'Bfrtip',
        });
}
</script>