<!DOCTYPE html>

<html lang="pt-br" class="default-style">

<head>
  <title>SGE</title>

  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="IE=edge,chrome=1">
  <meta name="description" content="">
  <meta name="viewport"
    content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

  {{-- favicon --}}
  <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/img/favicon/apple-touch-icon.png')}}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{asset('assets/img/favicon/favicon-32x32.png')}}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/img/favicon//favicon-16x16.png')}}">
  <link rel="manifest" href="{{asset('assets/img/favicon//site.webmanifest')}}">
  <link rel="icon" type="image/x-icon" href="{{asset('assets/img/favicon/favicon.ico')}}">

  <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900" rel="stylesheet">

  <!-- Icon fonts -->
  <link rel="stylesheet" href="{{asset('assets/vendor/fonts/fontawesome.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/fonts/ionicons.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/fonts/linearicons.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/fonts/open-iconic.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/fonts/pe-icon-7-stroke.css')}}">

  <!-- Core stylesheets -->
  <link rel="stylesheet" href="{{asset('assets/vendor/css/rtl/bootstrap.css')}}" class="theme-settings-bootstrap-css">
  <link rel="stylesheet" href="{{asset('assets/vendor/css/rtl/appwork.css')}}" class="theme-settings-appwork-css">
  <link rel="stylesheet" href="{{asset('assets/vendor/css/rtl/theme-corporate.css')}}" class="theme-settings-theme-css">
  <link rel="stylesheet" href="{{asset('assets/vendor/css/rtl/colors.css')}}" class="theme-settings-colors-css">
  <link rel="stylesheet" href="{{asset('assets/vendor/css/rtl/uikit.css')}}">
  


  <link rel="stylesheet" href="{{asset('assets/vendor/libs/datatables/datatables.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/demo.css')}}">
  <link href="https://fonts.googleapis.com/css?family=News+Cycle&display=swap" rel="stylesheet">


  <!-- Load polyfills -->
  <script src="{{asset('assets/vendor/js/polyfills.js')}}"></script>
  <script>
    document['documentMode']===10&&document.write('<script src="https://polyfill.io/v3/polyfill.min.js?features=Intl.~locale.en"><\/script>')
  </script>

  <script src="{{asset('assets/vendor/js/material-ripple.js')}}"></script>
  <script src="{{asset('assets/vendor/js/layout-helpers.js')}}"></script>



  <!-- Core scripts -->
  <script src="{{asset('assets/vendor/js/pace.js')}}"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="{{asset('assets/vendor/libs/validate/validate.js')}}"></script>

  <!-- Libs -->
  <link rel="stylesheet" href="{{asset('assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css')}}">
  
  <link rel="stylesheet" href="{{asset('assets/vendor/libs/flatpickr/flatpickr.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/libs/growl/growl.css')}}">

  <link href="https://fonts.googleapis.com/css?family=News+Cycle&display=swap" rel="stylesheet">


  <style>
      .authentication-wrapper {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-preferred-size: 100%;
    flex-basis: 100%;
    /* min-height: 100vh; */
    width: 100%
}

.authentication-wrapper .authentication-inner {
    width: 100%
}

.authentication-wrapper.authentication-1,
.authentication-wrapper.authentication-2,
.authentication-wrapper.authentication-4 {
    -ms-flex-align: center;
    align-items: center;
    -ms-flex-pack: center;
    justify-content: center
}

.authentication-wrapper.authentication-1 .authentication-inner {
    max-width: 300px
}

.authentication-wrapper.authentication-2 .authentication-inner {
    max-width: 380px
}

.authentication-wrapper.authentication-3 {
    -ms-flex-align: stretch;
    align-items: stretch;
    -ms-flex-pack: stretch;
    justify-content: stretch
}

.authentication-wrapper.authentication-3 .authentication-inner {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: stretch;
    align-items: stretch;
    -ms-flex-wrap: nowrap;
    flex-wrap: nowrap;
    -ms-flex-pack: stretch;
    justify-content: stretch
}

.authentication-wrapper.authentication-4 .authentication-inner {
    max-width: 800px
}

@media all and (-ms-high-contrast: none),
(-ms-high-contrast: active) {
    .authentication-wrapper:after {
        content: '';
        display: block;
        -ms-flex: 0 0 0%;
        flex: 0 0 0%;
        min-height: inherit;
        width: 0;
        font-size: 0
    }
}
.logo-login {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    /* background-color: #333; */
}

.logo-login div {     
    width: 90px;
    height: 110px;
}
.logo-login span {
    width: 70%;
}
.logo-login span h3{
    
    font-size: 26px;
    text-align: right;
    color: #9c9c9c;
    line-height: 130%;
}

    .modal-body .table tbody tr td.center {
      text-align: center;
    }

    .modal-icon-plus,
    .modal-icon-minus {
      font-size: 12px;
    }

    .table th,
    .table td {
      vertical-align: middle;
    }

    .modal-icon-plus {
      color: #0a77e8;
    }

    .modal-icon-minus {
      color: #fc5a5c;
    }

    .table-etiqueta-print {
      display: none;
    }

    .table-etiqueta-print table {
      width: 100%;
    }

    #layout-sidenav {
      background-color: #1762ad !important;
    }

    .sidenav.bg-blue .sidenav-item.active>.sidenav-link:not(.sidenav-toggle) {
      background-color: #0a77e8 !important;
    }

    .sidenav.bg-blue .sidenav-item.open:not(.sidenav-item-closing)>.sidenav-toggle,
    .sidenav.bg-blue .sidenav-item.active>.sidenav-link {
      color: #fff;
    }

    .sidenav.bg-dark .sidenav-link,
    .sidenav.bg-dark .sidenav-horizontal-prev,
    .sidenav.bg-dark .sidenav-horizontal-next {
      color: #cecece;
    }

    .app-brand-logo.demo {
      display: -ms-flexbox;
      display: flex;
      width: 45px;
      height: 45px;
      border-radius: 50%;
      -ms-flex-align: center;
      align-items: center;
      -ms-flex-pack: center;
      justify-content: center;
    }

    div.nav-tabs-left>ul>li a.active {
      font-weight: bold;

    }


    .default-style .flatpickr-calendar.open {
      z-index: 10000 !important;

    }

    .invalid {
      visibility: hidden !important;
    }

    td em {
      visibility: hidden !important;
      display: none !important;
    }

    #tabs-etiqueta {
      margin-bottom: 1.0em !important;
    }

    .modal-body,
    .modal-footer,
    .modal-header {
      padding: 1.0em !important;
    }
    #datatables-receitas tbody tr:hover td{
      background-color: #FFF;

    }
    .test-date{
      color: #F00;
    }
    .bold{
      font-weight: 500;
    }
  </style>

</head>

<body>
  <div class="page-loader">
    <div class="bg-primary"></div>
  </div>

  <!-- Layout wrapper -->
  <div class="layout-wrapper layout-1">
    <div class="layout-inner">



      <!-- Layout container -->
      <div class="layout-container">



        <!-- Layout content -->
        <div class="layout-content">


          <!-- Content -->
          @yield('content')
          <!-- / content -->



        </div>
        <!-- Layout content -->

        <!-- Layout footer -->
      </div>
        <nav class="layout-footer footer bg-footer-theme">
          <div class="container-fluid d-flex flex-wrap justify-content-between text-center container-p-x pb-3">
            <div class="pt-3">
              <span class="footer-text font-weight-bolder">Prefeitura Municipal de Teófilo Otoni</span> ©2020
            </div>
            <div>
              {{-- <a href="javascript:void(0)" class="footer-link pt-3">About Us</a>
                <a href="javascript:void(0)" class="footer-link pt-3 ml-4">Help</a>
                <a href="javascript:void(0)" class="footer-link pt-3 ml-4">Contact</a>
                <a href="javascript:void(0)" class="footer-link pt-3 ml-4">Terms &amp; Conditions</a> --}}
            </div>
          </div>
        </nav>
        <!-- / Layout footer -->

      </div>
      <!-- / Layout container -->

    </div>

    <!-- Overlay -->
    <div class="layout-overlay layout-sidenav-toggle"></div>
  </div>
  <!-- / Layout wrapper -->

  <!-- Core scripts -->
  <script src="{{asset('assets/vendor/libs/popper/popper.js')}}"></script>
  <script src="{{asset('assets/vendor/js/bootstrap.js')}}"></script>
  <script src="{{asset('assets/vendor/js/sidenav.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/datatables/datatables.js')}}"></script>
  <!-- Libs -->
  <script src="{{asset('assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/moment/moment.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/flatpickr/flatpickr.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/flatpickr/pt.js')}}"></script>

  <script src="{{asset('assets/vendor/libs/vanilla-text-mask/vanilla-text-mask.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/vanilla-text-mask/text-mask-addons.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/growl/growl.js')}}"></script>
  <script>






  </script>

  <!-- Demo -->
  <script src="{{asset('assets/js/demo.js')}}"></script>


</body>

</html>