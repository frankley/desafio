<!DOCTYPE html>

<html lang="pt-br" class="default-style">

<head>
  <title>SGE</title>

  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="IE=edge,chrome=1">
  <meta name="description" content="">
  <meta name="viewport"
    content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

  {{-- favicon --}}
  <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/img/favicon/apple-touch-icon.png')}}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{asset('assets/img/favicon/favicon-32x32.png')}}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/img/favicon//favicon-16x16.png')}}">
  <link rel="manifest" href="{{asset('assets/img/favicon//site.webmanifest')}}">
  <link rel="icon" type="image/x-icon" href="{{asset('assets/img/favicon/favicon.ico')}}">

  <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900" rel="stylesheet">

  <!-- Icon fonts -->
  <link rel="stylesheet" href="{{ asset('assets/vendor/fonts/fontawesome.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/fonts/ionicons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/fonts/linearicons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/fonts/open-iconic.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/fonts/pe-icon-7-stroke.css') }}">

  <!-- Core stylesheets -->
  <link rel="stylesheet" href="{{ asset('assets/vendor/css/bootstrap.css') }}"> 
  <link rel="stylesheet" href="{{ asset('assets/vendor/css/appwork.css') }}"> 
  <link rel="stylesheet" href="{{ asset('assets/vendor/css/theme-corporate.css') }}"> 
  <link rel="stylesheet" href="{{ asset('assets/vendor/css/colors.css') }}"> 
  <link rel="stylesheet" href="{{ asset('assets/vendor/css/uikit.css') }}">
  <link rel="stylesheet" href="{{asset('assets/vendor/libs/growl/growl.css')}}">
  
  <script src="{{ asset('assets/vendor/js/material-ripple.js') }}"></script>
  <script src="{{ asset('assets/vendor/js/layout-helpers.js') }}"></script>
  

  <!-- Core scripts -->
  <script src="{{ asset('assets/vendor/js/pace.js') }}"></script>
  <script src="{{ asset('assets/vendor/js/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/libs/input-mask/inputmask.js')}}"></script>
  <!-- Libs -->
  <link rel="stylesheet" href="{{ asset('assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/libs/datatables/datatables.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/libs/bootstrap-sweetalert/bootstrap-sweetalert.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/libs/toastr/toastr.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/libs/typeahead-js/typeahead.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/libs/bootstrap-datepicker/bootstrap-datepicker.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/libs/bootstrap-daterangepicker/bootstrap-daterangepicker.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/libs/bootstrap-material-datetimepicker/bootstrap-material-datetimepicker.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/libs/timepicker/timepicker.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/libs/minicolors/minicolors.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/libs/bootstrap-markdown/bootstrap-markdown.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/libs/bootstrap-sweetalert/bootstrap-sweetalert.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/libs/sweetalert2/sweetalert2.css')}}"> 
  <link rel="stylesheet" href="{{asset('assets/vendor/libs/spinkit/spinkit.css')}}">

  <!-- App -->
  <link rel="stylesheet" href="{{ asset('assets/css/demo.css') }}">  

  <style>
    .modal-body .table tbody tr td.center {
      text-align: center;
    }

    .modal-icon-plus,
    .modal-icon-minus {
      font-size: 12px;
    }

    .table th,
    .table td {
      vertical-align: middle;
    }

    .modal-icon-plus {
      color: #0a77e8;
    }

    .modal-icon-minus {
      color: #fc5a5c;
    }

    #layout-sidenav {
      /* background-color: #1762ad !important; */
      background-color: #352460 !important;

    }

    .sidenav.bg-blue .sidenav-item.active>.sidenav-link:not(.sidenav-toggle) {
      background-color: #352460 !important;
    }

    .sidenav.bg-blue .sidenav-item.open:not(.sidenav-item-closing)>.sidenav-toggle,
    .sidenav.bg-blue .sidenav-item.active>.sidenav-link {
      color: #fff;
    }

    .sidenav.bg-dark .sidenav-link,
    .sidenav.bg-dark .sidenav-horizontal-prev,
    .sidenav.bg-dark .sidenav-horizontal-next {
      color: #cecece;
      background-color: #352460 !important;
    }

    .bg-darken {
      background-color: #2f1e57 !important; 
      /* // 2f1e57  391e61 */
      color: #cecece !important;

    }

    .bg-darken a {
      color: #fff;
    }

    .app-brand-logo.demo {
      display: -ms-flexbox;
      display: flex;
      width: 45px;
      height: 45px;
      border-radius: 50%;
      -ms-flex-align: center;
      align-items: center;
      -ms-flex-pack: center;
      justify-content: center;
    }

    div.nav-tabs-left>ul>li a.active {
      font-weight: bold;

    }


    .default-style .flatpickr-calendar.open {
      z-index: 10000 !important;

    }

    .invalid {
      visibility: hidden !important;
    }



    .modal-body,
    .modal-footer,
    .modal-header {
      padding: 1.0em !important;
    }


    .bold {
      font-weight: 500;
    }
  </style>
  <link rel="stylesheet" href="{{asset('assets/css/app/cartoes.css')}}">
</head>

<body>
  <div class="page-loader">
    <div class="bg-primary"></div>
  </div>

  <!-- Layout wrapper -->
  <div class="layout-wrapper layout-1 layout-without-sidenav">
    <div class="layout-inner">

      <!-- Layout navbar -->
      <nav class="layout-navbar navbar navbar-expand-lg align-items-lg-center bg-white container-p-x" id="layout-navbar">

        <!-- Brand demo (see assets/css/demo/demo.css) -->
        <a href="index.html" class="navbar-brand app-brand demo d-lg-none py-0 mr-4">
          <span class="app-brand-logo demo bg-primary">
            <svg viewBox="0 0 148 80" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><linearGradient id="a" x1="46.49" x2="62.46" y1="53.39" y2="48.2" gradientUnits="userSpaceOnUse"><stop stop-opacity=".25" offset="0"></stop><stop stop-opacity=".1" offset=".3"></stop><stop stop-opacity="0" offset=".9"></stop></linearGradient><linearGradient id="e" x1="76.9" x2="92.64" y1="26.38" y2="31.49" xlink:href="#a"></linearGradient><linearGradient id="d" x1="107.12" x2="122.74" y1="53.41" y2="48.33" xlink:href="#a"></linearGradient></defs><path style="fill: #fff;" transform="translate(-.1)" d="M121.36,0,104.42,45.08,88.71,3.28A5.09,5.09,0,0,0,83.93,0H64.27A5.09,5.09,0,0,0,59.5,3.28L43.79,45.08,26.85,0H.1L29.43,76.74A5.09,5.09,0,0,0,34.19,80H53.39a5.09,5.09,0,0,0,4.77-3.26L74.1,35l16,41.74A5.09,5.09,0,0,0,94.82,80h18.95a5.09,5.09,0,0,0,4.76-3.24L148.1,0Z"></path><path transform="translate(-.1)" d="M52.19,22.73l-8.4,22.35L56.51,78.94a5,5,0,0,0,1.64-2.19l7.34-19.2Z" fill="url(#a)"></path><path transform="translate(-.1)" d="M95.73,22l-7-18.69a5,5,0,0,0-1.64-2.21L74.1,35l8.33,21.79Z" fill="url(#e)"></path><path transform="translate(-.1)" d="M112.73,23l-8.31,22.12,12.66,33.7a5,5,0,0,0,1.45-2l7.3-18.93Z" fill="url(#d)"></path></svg>
          </span>
          <span class="app-brand-text demo font-weight-normal ml-2">Desafio</span>
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#layout-navbar-collapse">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="navbar-collapse collapse" id="layout-navbar-collapse">
          <!-- Divider -->
          <hr class="d-lg-none w-100 my-2">

          <div class="navbar-nav align-items-lg-center ml-auto">
            <!-- Divider -->
            <div class="nav-item d-none d-lg-block text-big font-weight-light line-height-1 opacity-25 mr-3 ml-1">|</div>

            @if(!Auth::user())
            <div class="demo-navbar-notifications nav-item dropdown mr-lg-3">
              <a class="nav-link dropdown-toggle hide-arrow" href="{{url('login')}}">
                <i class="ion ion-md-contact navbar-icon align-middle"></i>
                Login
              </a>              
            </div>
            <div class="nav-item d-none d-lg-block text-big font-weight-light line-height-1 opacity-25 mr-3 ml-1">|</div>
            <div class="demo-navbar-notifications nav-item dropdown mr-lg-3">
              <a class="nav-link dropdown-toggle hide-arrow" href="{{url('register')}}">
                <i class="ion ion-md-person-add navbar-icon align-middle"></i>
                Registrar
              </a>              
            </div>
            @else
            <div class="demo-navbar-user nav-item dropdown">           
              <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
                <span class="d-inline-flex flex-lg-row-reverse align-items-center align-middle">
                  <img src="{{ Avatar::create(str_replace('.', ' ', Auth::user()->username))->toBase64() }}" alt class="d-block ui-w-30 rounded-circle">
                  <span class="px-1 mr-lg-2 ml-2 ml-lg-0">{{ strtoupper(Auth::user()->username) }}</span>
                </span>
              </a>
              <div class="dropdown-menu dropdown-menu-right">
                <a href="javascript:void(0)" class="dropdown-item"><i class="ion ion-ios-person text-lightest"></i> &nbsp; Meu Perfil</a>
                <div class="dropdown-divider"></div>
                <a href="{{url('logout')}}" class="dropdown-item"><i class="ion ion-ios-log-out text-danger"></i> &nbsp; Log Out</a>
              </div>              
            </div>
            @endif
          </div>
        </div>
      </nav>
      <!-- / Layout navbar -->

      <!-- Layout container -->
      <div class="layout-container">

        <!-- Layout content -->
        <div class="layout-content">

          <!-- Layout sidenav -->
          <div id="layout-sidenav" class="layout-sidenav-horizontal sidenav sidenav-horizontal flex-grow-0 bg-dark container-p-x">

            <!-- Links -->
            <ul class="sidenav-inner">

              <!-- Home -->
              <li class="sidenav-item">
                <a href="{{url('/')}}" class="sidenav-link"><i class="sidenav-icon ion ion-md-speedometer"></i>
                  <div>Home</div>
                </a>
              </li>

              <!-- Configurações -->
              <li class="sidenav-item active">
                <a href="{{url('configuracao')}}" class="sidenav-link"><i class="sidenav-icon ion ion-ios-albums"></i>
                  <div>Configurações</div>
                </a>
              </li>

              <li class="sidenav-item active">
                <a href="{{url('dicas')}}" class="sidenav-link"><i class="sidenav-icon ion ion-ios-albums"></i>
                  <div>Minhas Dicas</div>
                </a>
              </li>

            </ul>
          </div>
          <!-- / Layout sidenav -->

          <!-- Content -->
          <div class="container-fluid flex-grow-1 container-p-y">
            @yield('content')
          </div>
          <!-- / Content -->

          <!-- Layout footer -->
          <nav class="layout-footer footer bg-footer-theme">
            <div class="container-fluid d-flex flex-wrap justify-content-between text-center container-p-x pb-3">
              <div class="pt-3">
                <span class="footer-text font-weight-bolder">Desafio</span> ©
              </div>
              <div>
                <a href="javascript:void(0)" class="footer-link pt-3">Sobre Nós</a>
                <a href="javascript:void(0)" class="footer-link pt-3 ml-4">Contatos</a>
              </div>
            </div>
          </nav>
          <!-- / Layout footer -->

        </div>
        <!-- Layout content -->

      </div>
      <!-- / Layout container -->

    </div>
  </div>
  <!-- / Layout wrapper -->

  <!-- Core scripts -->
  <script src="{{asset('assets/vendor/libs/popper/popper.js')}}"></script>
  <script src="{{asset('assets/vendor/js/bootstrap.js')}}"></script>
  <script src="{{asset('assets/vendor/js/sidenav.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/datatables/datatables.js')}}"></script>
  <!-- Libs -->
  <script src="{{asset('assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/moment/moment.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/flatpickr/flatpickr.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/flatpickr/pt.js')}}"></script>

  <script src="{{asset('assets/vendor/libs/vanilla-text-mask/vanilla-text-mask.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/vanilla-text-mask/text-mask-addons.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/growl/growl.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/smartwizard/smartwizard.js')}}"></script>
  {{-- <script src="{{asset('assets/vendor/libs/bootstrap-multiselect/bootstrap-multiselect.js')}}"></script> --}}
  <script src="{{asset('assets/vendor/libs/bootstrap-multiselect/default-bootstrap-multiselect.js')}}"></script>
  {{-- <script src="{{asset('assets/vendor/libs/simple-mask-money/simple-mask-money.js')}}"></script> --}}
  <script src="{{asset('assets/vendor/libs/datatables/datatables.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/datatables/dataTables.scroller.min.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/ekko-lightbox/ekko-lightbox.min.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/vibrantjs/jquery.primarycolor.min.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/vibrantjs/Vibrant.min.js')}}"></script>  
  <script src="{{asset('assets/vendor/libs/block-ui/block-ui.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/sweetalert2/sweetalert2.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/select2/select2.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/bootstrap-select/bootstrap-select.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/bootstrap-multiselect/bootstrap-multiselect.js')}}"></script>
  

  <script src="{{asset('assets/js/app/helper.js')}}"></script>
  <script src="{{asset('assets/js/app/pagination.js')}}"></script>
  <script src="{{ asset('assets/vendor/libs/spin/spin.js') }}"></script>


</body>

</html>