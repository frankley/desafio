{{-- @dd($modelos) --}}
<div class="form-group">
    <label for="">Modelo</label>
    <select class="form-control" name="modelo_id" id="select-dica-edit-modelo_id">
      <option>Selecione o modelo...</option>
      @foreach ($modelos as $modelo)
      <option value="{{$modelo->id}}" @if($modelo->id == $modelo_dica_id) selected @endif>{{$modelo->nome}} - {{$modelo->ano}} - {{$modelo->combustivel}}</option>
      @endforeach
    </select>
  </div>