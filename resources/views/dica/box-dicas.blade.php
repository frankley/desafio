@foreach ($dicas as $dica)
<div class="col-md-6 card">
    <div class="card-header"><a href="javascript:void(0)" class="text-body text-large font-weight-semibold">{{$dica->modelo}} - {{$dica->ano}} - {{$dica->combustivel}}</a></div>
    <div class="d-flex flex-wrap mt-0 card-body style="flex: 0"">
        <div class="mr-3"><i class="vacancy-tooltip ion ion-md-car text-light"></i>&nbsp;{{$dica->marca}}</div>
        <div class="mr-3"><i class="vacancy-tooltip ion ion-md-time text-primary" title="Employment"></i>&nbsp;{{$dica->created}}</div>
    </div>
    <div class="mt-0 mb-1">
        {{$dica->descricao}}
    </div>
    <hr>
    <div class="row form-group">
        <div class="col-md form-inline d-flex justify-content-end">
            <button class="btn btn-warning btn-dica-excluir-{{$dica->dica_id}}" data-dica_id="{{$dica->dica_id}}"><i class="ion ion-ios-trash d-block">Excluir</i></button>
            &ensp;
            <button class="btn btn-info btn-dica-editar-{{$dica->dica_id}}" data-dica_id="{{$dica->dica_id}}"><i class="ion ion-ios-trash d-block">Editar</i></button>
        </div>
    </div>
</div><hr class="border-light m-0">
<script>
$(".btn-dica-excluir-{{$dica->dica_id}}").click(function(){
    let dica_id = $(this).data('dica_id');
    $.ajax({
        type: "GET",
        url: "{{url('dicas/delete')}}",
        data: {
            id: dica_id
        },
        success: function(){
            alert('excluido com sucesso');
        },
        complete: function(){
            minhasDicas();
        }
    })
})

$(".btn-dica-editar-{{$dica->dica_id}}").click(function(){
        let dica_id = $(this).data('dica_id');
        $.ajax({
            type: "GET",
            url: "{{url('dicas/editar')}}",
            data: {
                id: dica_id
            },
            success: function(data){
                // console.log(data.modelo_id);
                $("#edit-dica_id").val(data.dica_id);
                $("#edit-descricao").val(data.descricao)
                $("#select-edit-dica-marca").val(data.marca_id).change();
                modelos_dicas(data.marca_id, data.modelo_id)
            },
        })

    $("a.nav-link.active").removeClass('active');
    $("div.tab-pane.fade.active.show").removeClass('active show')
    $("#navs-top-dicas-editar-link").addClass('active');
    $("#navs-top-editar-dicas").addClass('active show')
    $(".d-none-atualizar").removeClass('d-none');
})

function modelos_dicas(marca_id, modelo_dica_id){
    $.ajax({
            type: "GET",
            url: "{{url('dicas/modelos')}}",
            data: {
                marca_id: marca_id,
                modelo_dica_id: modelo_dica_id
            },
            success: function(data){
                $("#dicas-modelos").html(data);
            },
        })
}
</script>
@endforeach