@extends('layouts.app')
@section('content')
<div class="nav-tabs-top mb-4">
    <ul class="nav nav-tabs">
      <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#navs-top-dicas" id="navs-top-dicas-link">Cadastrar Dicas</a>
      </li>
      <li class="nav-item d-none d-none-atualizar">
        <a class="nav-link" data-toggle="tab" href="#navs-top-editar-dicas" id="navs-top-dicas-editar-link">Atualizar Dicas</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#navs-top-minhas-dicas" id="navs-top-minhas-dicas-link">Minhas Dicas</a>
      </li>
    </ul>
    <div class="tab-content">
      <!--dicas-->
      <div class="tab-pane fade active show" id="navs-top-dicas">
        <div class="card-body">

            <form id="form-dicas">
                @csrf
                <div class="row form-group">
                    <div class="col-md">
                        <div class="form-group">
                          <label for="">Marcas</label>
                          <select class="form-control" name="marca_id" id="select-dica-marca">
                            <option disabled selected>Selecione a marca...</option>
                            @foreach ($marcas as $marca)
                                <option value="{{$marca->id}}">{{$marca->nome}}</option> 
                            @endforeach
                          </select>
                        </div>
                    </div>

                    <div class="col-md">
                        <div class="form-group">
                          <label for="">Modelo</label>
                          <select class="form-control" name="modelo_id" id="dica-modelo_id">
                            <option>Selecione o modelo...</option>
                          </select>
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md">
                        <label for="">Dica</label>
                        <textarea class="form-control" name="descricao" id="" rows="5" placeholder="Insira a sua dica sobre o modelo Selecionado"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md d-flex justify-content-end">
                        <button class="btn btn-info" type="submit">Cadastrar</button>
                    </div>
                </div>
            </form>
        </div>
      </div>

      <!--Editar dicas-->
      <div class="tab-pane fade" id="navs-top-editar-dicas">
        <div class="card-body">

            <form id="form-editar-dicas">
                @csrf
                <div class="row form-group">
                  <input type="text" name="dica_id" id="edit-dica_id" hidden>
                    <div class="col-md">
                        <div class="form-group">
                          <label for="">Marcas</label>
                          <select class="form-control" name="marca_id" id="select-edit-dica-marca">
                            <option disabled selected>Selecione a marca...</option>
                            @foreach ($marcas as $marca)
                                <option value="{{$marca->id}}">{{$marca->nome}}</option> 
                            @endforeach
                          </select>
                        </div>
                    </div>

                    <div class="col-md" id="dicas-modelos">
                        
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md">
                        <label for="">Dica</label>
                        <textarea class="form-control" name="descricao" id="edit-descricao" rows="5" placeholder="Insira a sua dica sobre o modelo Selecionado"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md d-flex justify-content-end">
                        <button class="btn btn-info" type="submit">Atualizar</button>
                    </div>
                </div>
            </form>
        </div>
      </div>

      <!--minhas-dicas-->
      <div class="tab-pane fade" id="navs-top-minhas-dicas">
        <div class="card-header">
          <h4 class="font-weight-bold py-3 mb-0">
            <span class="text-muted font-weight-light">Home /</span> Dicas
          </h4>
          <div class="row">
            <div class="col-md">
              <form id="form-dicas-filtro-logado">
                <div class="form-group">
                    <div class="input-group">
                    <select class="form-control" name="marca_id" id="filtro-dicas-marca_id">
                        <option value="" selected disabled>Selecione a Marca</option>
                        @foreach ($marcas as $mar)
                        <option value="{{$mar->id}}">{{$mar->nome}}</option>
                        @endforeach
                    </select>
                    <select class="form-control" name="modelo_id" id="filtro-dicas-modelo_id">
                        <option value="" selected disabled>Selecione o Modelo</option>
                    </select>
                    <span class="input-group-append">
                        <button class="btn btn-secondary" type="submit">Buscar</button>
                    </span>
                    </div>
                </div>
            </form>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="row box-dicas">
          </div>
        </div>
      </div>

    </div>
  </div>  
  

@include('dica.jquery')
@endsection