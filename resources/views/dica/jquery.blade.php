<script>
$('#select-dica-marca').change(function(){
    dicaModelos($(this).val(), "#dica-modelo_id");
})

$("#select-edit-dica-marca").change(function(){
    dicaModelos($(this).val(), "#select-dica-edit-modelo_id");
})

function dicaModelos(marca_id, chave){
    // alert(marca_id);
    var chave = chave;
    $(chave).html('<option disabled selected>Selecione o Modelo...</option>')
    $.ajax({
        type: "GET",
        url: "{{url('configuracao/modelos/lista')}}",
        data: {
            marca_id: marca_id
        },
        success: function(data){
            // console.log(data);
            $(data).each(function(key, dados){
                $(chave).append("<option id='option-modelo-"+dados.modelo_id+"' value='"+dados.modelo_id+"'>"+dados.nome+' - '+dados.ano+' - '+dados.combustivel+"</option>");
            })
        }
    })
}

$('#form-dicas').on('submit', function(e){
    e.preventDefault();
    Swal.fire({
        icon: "warning",
        title: 'Cadastro de Dicas',
        html: "Deseja salvar a Dica?",
        showCancelButton: true,
        confirmButtonText: 'Sim, Enviar!',
        confirmButtonColor: '#FF8B18',
        cancelButtonText: 'Não'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: "{{url('dicas/save')}}",
                data: $(this).serialize(),
                beforeSend: function(){
                    loaderBlockUI('Salvando Dica...')
                },
                success: function(data){
                    loaderUnBlockUI();
                    if(data.codigo == 0){
                      $('#form-dicas').trigger("reset");
                        growlSuccess('Dica', data.msn)
                    }else{
                      $('#form-dicas').trigger("reset");
                        growlError('Erro ao salvar Dica', data.msn)
                    }
                }
            });
        }
    });  
})

$("#navs-top-minhas-dicas-link").click(function(){
    minhasDicas('');
    $("#d-none-atualizar").addClass('d-none');
})
function minhasDicas(filtro_dicas){
    $(".box-dicas").html("");
    $.ajax({
        type: "GET",
        url: "{{url('dicas/lista')}}",
        data: filtro_dicas,
        success: function(data){
            // console.log(data);
            if(data != ''){
                    $(".box-dicas").append(data);
            }else{
                $(".box-dicas").html('<div class="col-md">Nenhuma dica Cadastrada por Você</div>')
            }
        }
    })
}

$('#filtro-dicas-marca_id').change(function(){
    // alert($(this).val());
    dicaModelos($(this).val(), "#filtro-dicas-modelo_id");
})

$("#form-dicas-filtro-logado").on("submit", function(e){
    e.preventDefault();
    var filtro_dicas = $(this).serialize();
    minhasDicas(filtro_dicas)
})

$('#form-editar-dicas').on('submit', function(e){
    e.preventDefault();
    Swal.fire({
        icon: "warning",
        title: 'Atualizar Dicas',
        html: "Deseja atualizar a Dica?",
        showCancelButton: true,
        confirmButtonText: 'Sim, Enviar!',
        confirmButtonColor: '#FF8B18',
        cancelButtonText: 'Não'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: "{{url('dicas/update')}}",
                data: $(this).serialize(),
                beforeSend: function(){
                    loaderBlockUI('Atualizando Dica...')
                },
                success: function(data){
                    loaderUnBlockUI();
                    if(data.codigo == 0){
                    //   $('#form-dicas').trigger("reset");
                        growlSuccess('Dica', data.msn)
                    }else{
                    //   $('#form-dicas').trigger("reset");
                        growlError('Erro ao Atualizar Dica', data.msn)
                    }
                }
            });
        }
    }); 
})
</script>