<!DOCTYPE html>
<html lang="pt-br">

<head>
     <meta charset="utf-8" />
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
     <meta name="description" content="" />
     <meta name="author" content="" />

     <!-- Title -->
     <title>Acesso Negado</title>
     <link rel="stylesheet" href="{{asset('assets/vendor/fonts/fontawesome.css')}}">
     <link rel="stylesheet" href="{{asset('assets/vendor/css/rtl/bootstrap.css')}}" class="theme-settings-bootstrap-css">
     <script src="{{asset('assets/vendor/js/jquery.min.js')}}"></script>
</head>

<body class="bg-dark text-white py-5">
     <div class="container py-5">
          <div class="row">
               <div class="col-md-5 text-right">
                    <p><i class="fa fa-exclamation-triangle fa-5x" style="color:#FAFF0D"></i><br/>Status Code: 403</p>
               </div>
               <div class="col-md">
                    <h3>Acesso Negado!</h3>
                    <p>Você não tem permissão para acessar está página.<br/>Entre em contado com o Administrador do Sistema</p>
               </div>
          </div>
     </div>

     <div id="footer" class="text-center">
        <div class="pt-3">
            <span class="footer-text font-weight-bolder">Farmácia Indiana</span> ©2020
          </div>
     </div>
<script>
     setInterval(function(){ 
          window.location.href = "{{url('logout')}}"
     }, 5000);
</script>
</body>
<script src="{{asset('assets/vendor/js/bootstrap.js')}}"></script>
</html>