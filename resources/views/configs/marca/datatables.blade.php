<!--  Datatables -->
<div class="table-responsive ">
  <table id="datatables-marcas" class="table table-striped">
    <thead class="thead-light">
      <tr>
        <th width="80%">Marcas</th>   
        <th>Ações</th>     
      </tr>
    </thead>
    <tbody>
    @foreach ($marcas as $marca)
    <tr>
        <td>{{$marca->nome}}</td>
        <td>
          <button class="btn btn-danger btn-del-marca" data-marca_id="{{$marca->id}}"><i class="fa fa-trash"></i></button>
          <button class="btn btn-info btn-edit-marca" data-marca_id="{{$marca->id}}"><i class="fas fa-edit"></i></button>
        </td>
    </tr> 
    @endforeach
    </tbody>
  </table>
</div>
<!-- / Datatables -->

<script>

    function carregaMarcas(){
      $.fn.dataTable.ext.errMode = 'throw';
      if ( $.fn.dataTable.isDataTable( '#datatables-marcas' ) ) {
              mainTable.destroy();
      }

      mainTable = $('#datatables-marcas').DataTable( {
          language: {
              url: "{{asset('assets/localisation/Portuguese-Brasil.json')}}"    
          },
          dom: 'Bfrtip',
      });
    }

    $(".btn-del-marca").click(function(){
      let marca_id = $(this).data('marca_id')
      $.ajax({
        type: "GET",
        url: "{{url('configuracao/marca/delete')}}",
        data:{
          marca_id: marca_id
        },
        complete: function(){
          marcas();
        }
      })
    })

    $(".btn-edit-marca").click(function(){
      let marca_id = $(this).data('marca_id')
      $.ajax({
        type: "GET",
        url: "{{url('configuracao/marca/editar')}}",
        data:{
          marca_id: marca_id
        },
        success: function(data){
          $("#box-edicao-marca").html(data);
        },
        complete: function(){
          $("#modalMarcaEdicao").modal('show');
        }
      })
    })
</script>