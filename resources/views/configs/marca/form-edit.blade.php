<form id="form-marca-update">
@csrf
    <div class="form-group">
        <div class="input-group">
        <input type="hidden" name="marca_id" value="{{$marca->id}}">
        <input type="text" class="form-control" placeholder="Inserir Marca" name="nome" value="{{$marca->nome}}">
        <span class="input-group-append">
            <button class="btn btn-secondary" type="submit">Atualizar</button>
        </span>
        </div>
    </div>
</form>

<script>
$('#form-marca-update').on('submit', function(e){
    e.preventDefault();
    $("#modalMarcaEdicao").modal('hide');
    Swal.fire({
        icon: "warning",
        title: 'Atualizar Marca',
        html: "Deseja atualizar a Marca?",
        showCancelButton: true,
        confirmButtonText: 'Sim, Atualizar!',
        confirmButtonColor: '#FF8B18',
        cancelButtonText: 'Não'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: "{{url('configuracao/marca/update')}}",
                data: $(this).serialize(),
                beforeSend: function(){
                    loaderBlockUI('Atualizando Marca...')
                },
                success: function(data){
                    loaderUnBlockUI();
                    if(data.codigo == 0){
                        marcas();
                        growlSuccess('Marca', data.msn)
                    }else{
                        growlError('Erro ao salvar Marca', data.msn)
                    }
                }
            });
        }
    });  
    
})
</script>