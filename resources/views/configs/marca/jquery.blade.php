<script>
$('#form-marca').on('submit', function(e){
    e.preventDefault();
    
    var msn = 'Marca'
    Swal.fire({
        icon: "warning",
        title: 'Cadastro de Marca',
        html: "Deseja salvar a Marca?",
        showCancelButton: true,
        confirmButtonText: 'Sim, Enviar!',
        confirmButtonColor: '#FF8B18',
        cancelButtonText: 'Não'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: "{{url('configuracao/marca/save')}}",
                data: $(this).serialize(),
                beforeSend: function(){
                    loaderBlockUI('Salvando Marca...')
                },
                success: function(data){
                    loaderUnBlockUI();
                    if(data.codigo == 0){
                      $('#form-marca').trigger("reset");
                        marcas();
                        growlSuccess('Marca', data.msn)
                    }else{
                      $('#form-marca').trigger("reset");
                        growlError('Erro ao salvar Marca', data.msn)
                    }
                }
            });
        }
    });  
    
})

function marcas(){
  $.ajax({
    type: "GET",
    url: "{{url('configuracao/marcas/lista')}}",
    beforeSend: function(){
        loaderBlockUI('Carregando Marcas...')
    },
    success: function(data){
        $("#marcas-datatables").html(data);
    },
    complete: function(){
      carregaMarcas();
      loaderUnBlockUI();
    }
  })
}
</script>