<!--  Datatables -->
<div class="table-responsive ">
  <table id="datatables-modelos" class="table table-striped">
    <thead class="thead-light">
      <tr>
        <th>Modelos</th>        
        <th>Marca</th>
        <th>Combustivel</th>
        <th>Ano</th>
      </tr>
    </thead>
    <tbody>
    @foreach ($modelos as $modelo)
    <tr>
        <td>{{$modelo->nome}}</td>
        <td>{{$modelo->marca}}</td>
        <td>{{$modelo->combustivel}}</td>
        <td>{{$modelo->ano}}</td>
    </tr> 
    @endforeach
    </tbody>
  </table>
</div>
<!-- / Datatables -->

<script>
    $(document).ready(function(){
      carregaModelo();
    });
    function carregaModelo(){
      $.fn.dataTable.ext.errMode = 'throw';
      if ( $.fn.dataTable.isDataTable( '#datatables-modelos' ) ) {
              mainTable.destroy();
      }

      mainTable = $('#datatables-modelos').DataTable( {
              language: {
                  url: "{{asset('assets/localisation/Portuguese-Brasil.json')}}"    
              },
              dom: 'Bfrtip',
          });
    }
</script>