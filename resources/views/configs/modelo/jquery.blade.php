<script>
$('#form-modelo').on('submit', function(e){
    e.preventDefault();
    // console.log($(this).serialize());
    var msn = 'Modelo'
    Swal.fire({
        icon: "warning",
        title: 'Cadastro de Modelo',
        html: "Deseja salvar o Modelo?",
        showCancelButton: true,
        confirmButtonText: 'Sim, Enviar!',
        confirmButtonColor: '#FF8B18',
        cancelButtonText: 'Não'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: "{{url('configuracao/modelo/save')}}",
                data: $(this).serialize(),
                beforeSend: function(){
                    loaderBlockUI('Salvando Modelo...')
                },
                success: function(data){
                    loaderUnBlockUI();
                    if(data.codigo == 0){
                      $('#form-Modelo').trigger("reset");
                        modelos()
                        growlSuccess('Modelo', data.msn)
                    }else{
                      $('#form-modelo').trigger("reset");
                        growlError('Erro ao salvar Modelo', data.msn)
                    }
                }
            });
        }
    });  
    
})

function modelos(){
  $.ajax({
    type: "GET",
    url: "{{url('configuracao/modelos/lista')}}",
    beforeSend: function(){
        loaderBlockUI('Carregando Modelos...')
    },
    success: function(data){
        $("#modelos-datatables").html(data);
    },
    complete: function(){
      loaderUnBlockUI();
    }
  })
}

$("#navs-top-modelo-link").click(function(){
  // alert('teste')
  $("#select-marca").html("<option value=''>Selecione a marca...</option>")
  marcasOption();
})

function marcasOption(){
  $.ajax({
    type: "GET",
    url: "{{url('configuracao/marcas/lista-opcao')}}",
    success: function(data){
        $(data).each(function(key, dados){
          console.log(dados.nome);
          $("#select-marca").append("<option value='"+dados.id+"'>"+dados.nome+"</option>")
          // $("#option-marca").append("teste")
        })
    },
  })
}
</script>