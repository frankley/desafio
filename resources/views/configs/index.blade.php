@extends('layouts.app')
@section('content')
<div class="nav-tabs-top mb-4">
    <ul class="nav nav-tabs">
      <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#navs-top-marca">Marcas</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#navs-top-modelo" id="navs-top-modelo-link">Modelos</a>
      </li>
    </ul>
    <div class="tab-content">
      <!--Marcas-->
      <div class="tab-pane fade active show" id="navs-top-marca">
        <div class="card-body">
            <div class="row">
                <div class="col-md">
                    <form id="form-marca">
                    @csrf
                        <div class="form-group">
                            <div class="input-group">
                            <input type="text" class="form-control" placeholder="Inserir Marca" name="nome" id="input-marca">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit" id="form-btn-marca">Salvar</button>
                            </span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div id="marcas-datatables"></div>
        </div>
      </div>

      <!--Modelos-->
      <div class="tab-pane fade" id="navs-top-modelo">
        <div class="card-body">
          <div class="row">
            <div class="col-md">
                <form id="form-modelo">
                @csrf
                    <div class="form-group">
                        <div class="input-group">
                        <input type="text" class="form-control" placeholder="Inserir Modelo" name="nome" required>
                        <select name="combustivel" class="form-control" required>
                          <option value="" disabled selected>Selecione o Combustivel</option>
                          <option value="gasolina">Gasolina</option>
                          <option value="alcool">Alcool</option>
                          <option value="diesel">Diesel</option>
                          <option value="eletrico">Eletrico</option>
                          <option value="alcool/gasolina">Alcool/Gasolina</option> 
                        </select>
                        <select name="marca_id" class="form-control" id="select-marca">
                          <option value=''>Selecione a marca...</option>
                        </select>
                        <select name="tipo" class="form-control" required>
                          <option value="" disabled selected>Selecione o Tipo...</option>
                          <option value="Carro">Carro</option>
                          <option value="Moto">Moto</option>
                        </select>
                        <input type="number" class="form-control" placeholder="Inserir Ano" name="ano" required>
                        <span class="input-group-append">
                            <button class="btn btn-secondary" type="submit">Salvar</button>
                        </span>
                        </div>
                    </div>
                </form>
            </div>
          </div>

          <div id="modelos-datatables"></div>
        </div>
      </div>

    </div>
  </div>  
  
 
  <!-- Modal -->
  <div class="modal fade" id="modalMarcaEdicao" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Edição Marca</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" id="box-edicao-marca"></div>
      </div>
    </div>
  </div>
<script>
$(document).ready(function(){
  marcas();
  modelos();
})
</script>
@include('configs.marca.jquery')
@include('configs.modelo.jquery')
@endsection