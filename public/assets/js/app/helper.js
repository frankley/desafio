function growlNotice(title, message) {
    $.growl.notice({
        title: title,
        message: message,
        size: "large",
        style: "default",
        duration: 5600
    });
}

function growlSuccess(title, message) {
    $.growl.notice({
        title: title,
        message: message,
        size: "large",
        style: "notice",
        duration: 5600
    });
}

function growlWarning(title, message) {
    $.growl.warning({
        title: title,
        message: message,
        size: "large",
        duration: 5600
    });
}

function growlError(title, message) {
    $.growl.error({
        title: title,
        message: message,
        size: "large",
        duration: 5600
    });
}

function loaderBlockUI(message="Aguarde...") {
    $.blockUI({
        message:
        '<div class="sk-cube-grid sk-primary">'+
            '<div class="sk-cube sk-cube1"></div>'+
            '<div class="sk-cube sk-cube2"></div>'+
            '<div class="sk-cube sk-cube3"></div>'+
            '<div class="sk-cube sk-cube4"></div>'+
            '<div class="sk-cube sk-cube5"></div>'+
            '<div class="sk-cube sk-cube6"></div>'+
            '<div class="sk-cube sk-cube7"></div>'+
            '<div class="sk-cube sk-cube8"></div>'+
            '<div class="sk-cube sk-cube9"></div>'+
        '</div>'+
            '</div><h3 style="color: #8A008A">'+message+'</h3>'
        ,

        css: {
            backgroundColor: "transparent",
            border: "0",
            zIndex: 9999999
        },
        overlayCSS: {
            backgroundColor: "#fff",
            opacity: 0.8,
            zIndex: 9999990
        }
    });
}
function loaderUnBlockUI() {
    $.unblockUI();
}