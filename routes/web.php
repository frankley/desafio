<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');

Route::get('/', function () {
    return redirect('login');
});


// Route::get('/', 'HomeController@index');
Route::group(['prefix' => '/'], function () {
    Route::get('/', 'HomeController@index'); 
    Route::get('lista', 'HomeController@listaDicas');   
    Route::get('modelos/lista', 'HomeController@listaModelos');
});

/**Grupo de rotas de Configurações **/
Route::group(['prefix' => 'configuracao', 'middleware' => 'auth'], function () {
    Route::get('/', 'ConfiguracaoController@index');
    Route::post('marca/save', 'ConfiguracaoController@saveMarca');
    Route::get('marcas/lista', 'ConfiguracaoController@listaMarcas');
    Route::get('marcas/lista-opcao', 'ConfiguracaoController@listaMarcasOption');
    Route::post('modelo/save', 'ConfiguracaoController@saveModelo');
    Route::get('modelos/lista', 'ConfiguracaoController@listaModelos');
    Route::get('marca/delete', 'ConfiguracaoController@deleteMarca');
    Route::get('marca/editar', 'ConfiguracaoController@editarMarca');
    Route::post('marca/update', 'ConfiguracaoController@updateMarca');
    
});
/**End Grupo de rotas com Configurações **/

Route::group(['prefix' => 'dicas', 'middleware' => 'auth'], function () {
    Route::get('/', 'DicaController@index');
    Route::post('save', 'DicaController@save');
    Route::get('lista', 'DicaController@listaDicas');
    Route::get('delete', 'DicaController@delete');
    Route::get('editar', 'DicaController@editar');
    Route::get('modelos', 'DicaController@modelos');
    Route::post('update', 'DicaController@update');
});