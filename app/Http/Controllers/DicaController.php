<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;

class DicaController extends Controller
{
    public function index(){
        $marcas = DB::table('marcas')->orderBy('nome', 'asc')->get();        
        return view('dica.index', compact('marcas'));
    }

    public function save(Request $request){
        $dica = collect($request->except('_token', 'marca_id'));
        $dica->put('user_id', Auth::user()->id);
        $dica->put('created_at', date('Y-m-d H:i:s'));

        try{
            $id = DB::table('dicas')->insertGetId($dica->toArray());
            if($id){
                $msn = [
                    'codigo' => 0,
                    'msn' => 'Salvo com sucesso'
                ];
            }
        }catch(\Exception $e){
            $msn = [
                'codigo' => 1,
                'msn' => $e->getMessage()
            ];
        }
        return response()->json($msn);
    }

    public function listaDicas(){
        $dicas = DB::table('dicas as d')
                    ->limit(10)
                    ->join('modelos as mod', 'mod.id', 'd.modelo_id')
                    ->join('marcas as mar', 'mar.id', 'mod.marca_id')
                    ->select('mod.nome as modelo', 'mod.combustivel',
                     'mar.nome as marca', 'mod.ano as ano', 'd.id as dica_id','d.descricao as descricao', 
                     DB::raw('DATE_FORMAT(d.created_at, "%d/%m/%Y %H:%i:%s") as created'))
                    ->where('user_id', Auth::user()->id);
        if($_GET){
            $dicas->where(function ($dicas){
                $dicas->where($_GET);
            });
        }        
        $dicas = $dicas->orderBy('d.created_at', 'desc')->get();
       
        return view('dica.box-dicas', compact('dicas'));
    }

    public function delete(){
        DB::table('dicas')->where('id', $_GET['id'])->delete();
        return response()->json('ok', 201);
    }

    public function editar(){
        $dica = DB::table('dicas as d')->where('d.id', $_GET['id'])
                    ->join('modelos as mod', 'mod.id', 'd.modelo_id')
                    ->join('marcas as mar', 'mar.id', 'mod.marca_id')
                    ->select('mod.nome as modelo', 'mod.combustivel',
                    'mar.nome as marca', 'mod.ano as ano', 'd.id as dica_id',
                    'd.descricao as descricao', 'mar.id as marca_id', 'mod.id as modelo_id')
                    ->first();
        return response()->json($dica);
    }

    public function modelos(){
        $modelos = DB::table('modelos')->where('marca_id', $_GET['marca_id'])->get();
        $modelo_dica_id = $_GET['modelo_dica_id'];
        return view('dica.modelo.option-modelo', compact('modelos', 'modelo_dica_id'));
    }

    public function update(Request $request){
        $dica = collect($request->except('_token', 'dica_id', 'marca_id'));
        $dica->put('user_id', Auth::user()->id);
        $dica->put('updated_at', date('Y-m-d H:i:s'));
        // dd($dica);
        try{
            $id = DB::table('dicas')->where('id', $request->dica_id)->update($dica->toArray());
            if($id){
                $msn = [
                    'codigo' => 0,
                    'msn' => 'Atualizado com sucesso'
                ];
            }
        }catch(\Exception $e){
            $msn = [
                'codigo' => 1,
                'msn' => $e->getMessage()
            ];
        }
        return response()->json($msn);
    }
    
}
