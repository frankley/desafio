<?php

namespace App\Http\Controllers;

use App\Modelo;
use Illuminate\Http\Request;
use DB;
class ConfiguracaoController extends Controller
{
    public function index(){
        return view('configs.index');
    }

    public function saveMarca(Request $request){
        try{
            $id = DB::table('marcas')->insertGetId($request->except('_token'));
            if($id){
                $msn = [
                    'codigo' => 0,
                    'msn' => 'Salvo com sucesso'
                ];
            }
        }catch(\Exception $e){
            $msn = [
                'codigo' => 1,
                'msn' => $e->getMessage()
            ];
        }
        
        return response()->json($msn);
    }

    public function listaMarcas(){
        $marcas = DB::table('marcas')->orderBy('nome', 'asc')->get();
        return view('configs.marca.datatables', compact('marcas'));
    }

    public function listaMarcasOption(){
        $marcas = DB::table('marcas')->orderBy('nome', 'asc')->get();
        return response()->json($marcas);
    }

    public function saveModelo(Request $request){
        // dd($request->except('_token'));
        try{
            $id = DB::table('modelos')->insertGetId($request->except('_token'));
            if($id){
                $msn = [
                    'codigo' => 0,
                    'msn' => 'Salvo com sucesso'
                ];
            }
        }catch(\Exception $e){
            $msn = [
                'codigo' => 1,
                'msn' => $e->getMessage()
            ];
        }
        
        return response()->json($msn);
    }

    public function listaModelos(){
        
        $modelos = DB::table('modelos as mod')
                        ->join('marcas as mar', 'mar.id', 'mod.marca_id')
                        ->select(
                                'mod.id as modelo_id','mod.nome as nome', 'mod.combustivel',
                                'mar.nome as marca', 'mod.ano as ano'
                        );
        if(isset($_GET['marca_id'])){
            $modelos->where('mod.marca_id', $_GET['marca_id']);
            $modelos = $modelos->orderBy('nome', 'asc')->get();
            return response()->json($modelos);
        }
        $modelos = $modelos->orderBy('nome', 'asc')->get();

        return view('configs.modelo.datatables', compact('modelos'));
    }

    public function deleteMarca(){
        DB::table('marcas')->where('id', $_GET['marca_id'])->delete();
        return response()->json('Marca deletada com sucesso!');
    }

    public function editarMarca(){
        $marca = DB::table('marcas')->where('id', $_GET['marca_id'])->first();
        return view('configs.marca.form-edit', compact('marca'));
    }

    public function updateMarca(Request $request){
        DB::table('marcas')->where('id', $request->marca_id)->update([
                    'nome' => $request->nome,
                    'updated_at'=> date('Y-m-d H:i:s')
        ]);
        $msn = [
            'codigo' => 0,
            'msn' => 'Salvo com sucesso'
        ];
        return response()->json($msn);
    }
}
