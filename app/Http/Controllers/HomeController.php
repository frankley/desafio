<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
class HomeController extends Controller
{
    public function index(){
        $marcas = DB::table('marcas')->orderBy('nome', 'asc')->get();
        return view('home', compact('marcas'));
    }

    public function listaDicas(){
        $dicas = DB::table('dicas as d')
                    ->join('modelos as mod', 'mod.id', 'd.modelo_id')
                    ->join('marcas as mar', 'mar.id', 'mod.marca_id')
                    ->select('d.id as id','mod.nome as modelo', 'mod.combustivel',
                     'mar.nome as marca', 'mod.ano as ano', 'd.descricao as descricao', 
                     DB::raw('DATE_FORMAT(d.created_at, "%d/%m/%Y %H:%i:%s") as created'));
        if($_GET){
            $dicas->where(function ($dicas){
                $dicas->where($_GET);
            });
        }
        $dicas = $dicas->orderBy('d.created_at', 'desc')->get();
        // $dicas = ;
        // return response()->json($dicas);
        return view('box-dicas', compact('dicas'));
    }

    public function listaModelos(){
        
        $modelos = DB::table('modelos as mod')
                        ->join('marcas as mar', 'mar.id', 'mod.marca_id')
                        ->select(
                                'mod.id as modelo_id','mod.nome as nome', 'mod.combustivel',
                                'mar.nome as marca', 'mod.ano as ano'
                        );
        if(isset($_GET['marca_id'])){
            $modelos->where('mod.marca_id', $_GET['marca_id']);
            $modelos = $modelos->orderBy('nome', 'asc')->get();
            return response()->json($modelos);
        }
    }
}
