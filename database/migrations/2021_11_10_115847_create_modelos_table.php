<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModelosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modelos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome');
            $table->unsignedBigInteger('marca_id');
            $table->foreign('marca_id')
                    ->references('id')->on('marcas');
            $table->enum('combustivel',['gasolina', 'alcool', 'eletrico', 'diesel', 'alcool/gasolina']);
            $table->enum('tipo',['Carro', 'Moto']);
            $table->string('ano');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modelos');
    }
}
