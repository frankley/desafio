<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Dica;
use Faker\Generator as Faker;

$factory->define(Dica::class, function (Faker $faker) {
    return [
        "user_id" => 1,
        "modelo_id" => rand(1,1000),
        "descricao" => $faker->paragraph($nbSentences = 3, $variableNbSentences = true) 
    ];
});
