<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Marca;

use Faker\Generator as Faker;

// $faker = \Faker\Factory::create('pt_BR');

$factory->define(Marca::class, function (Faker $faker) {
    return [
        "nome" => $this->faker->name,
    ];
});