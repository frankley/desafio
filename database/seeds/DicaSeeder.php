<?php

use Illuminate\Database\Seeder;

class DicaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('pt_BR');
        factory(App\Dica::class, 50)->create();
    }
}
