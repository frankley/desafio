<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
           0 => ['role-list','Listagem de Regras'],
           1 => ['role-create','Criação de Regras'],
           2 => ['role-edit','Edição de Regras'],
           3 => ['role-delete','Exclusão de Regras'],
           4 => ['user-list','Listagem de Usuários'],
           5 => ['user-create','Criação de Usuários'],
           6 => ['user-edit','Edição de Usuários'],
           7 => ['user-delete','Exclusão de Usuários'],
           8 => ['produto-list','Listagem de Produtos'],
           9 => ['produto-create','Criação de Produtos'],
           10 => ['produto-edit','Edição de Produtos'],
           11 => ['produto-delete','Exclusão de Produtos'],
           12 => ['tipo-list','Listagem de Tipos'],
           13 => ['tipo-create','Criação de Tipos'],
           14 => ['tipo-edit','Edição de Tipos'],
           15 => ['tipo-delete','Exclusão de Tipos'],
           16 => ['entrada-list','Listagem de Entradas'],
           17 => ['entrada-create','Criação de Entrada'],
           18 => ['entrada-edit','Edição de Entrada'],
           19 => ['entrada-delete','Exclusão de Entrada'],
           20 => ['saida-list','Listagem de Saídas'],
           21 => ['saida-create','Criação de Saídas'],
           22 => ['saida-edit','Edição de Saídas'],
           23 => ['saida-delete','Exclusão de Saídas'],
        ];

        $i = 0;
        foreach ($permissions as $permission) {
             Permission::create(
                 [
                 'name' => $permissions[$i][0], 
                 'description' => $permissions[$i][1]
                 ]);
        $i = $i+1;
        }
    }
}
